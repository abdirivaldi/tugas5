import React, {useEffect} from 'react'
import {View, Text, Image} from 'react-native'

const splashscreen = ({ navigation, route }) => {
    useEffect(() => {
      setTimeout(() => {
        navigation.navigate('authnavigation');
      }, 1000);
    }, []);
  
    return (
      <View style={{ flex: 1, backgroundColor: '#fff', justifyContent: 'center', alignItems: 'center' }}>
        <Image 
          source={require('./assets/image/Splash.png')} 
          style={{ width: 150, height: 150, resizeMode: 'contain' }} 
        />
      </View>
    );
  }
  
export default splashscreen;