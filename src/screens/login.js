import React from "react";
import { View, Text, ScrollView, 
         KeyboardAvoidingView, Image, TextInput, 
         TouchableOpacity, StyleSheet,Dimensions } from "react-native";

const login = ({navigation}) => {
    const toregister = () => {
        navigation.navigate('register');
      };
    return (    
    <View>
        <ScrollView>
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}
        <KeyboardAvoidingView>
        behavior="padding"
        keyboardVerticalOffset={-500}
        <Image 
        source={require('./assets/image/Login.png')}
        style={{width:Dimensions.get ('window'),
                height:317}}
        />
        <View style={{
                width:'100%',
                backgroundColor: '#fff',
                borderTopLeftRadius: 19,
                borderTopRightRadius: 19,
                paddingHorizontal: 20,
                paddingTop: 38,
                marginTop: -20
        }} />
        <Text style={{color:'red', fontWeight:'bold'}}> Email  </Text>
        <TextInput
        placeholder="Masukan email Anda"
        style=
        {{
            marginTop: 15,
            width: '100%',
            borderRadius: 8,
            backgroundColor: '#F6F8FF',
            paddingHorizontal: 10
        }}
        keyboardType="email-address"

        />
        <Text style={{color:'red', fontWeight:'bold'}}> Password </Text>
        <TextInput
        placeholder="Masukan password Anda"
        secureTextEntry={true}
        style= {{
            marginTop: 15,
            width: '100%',
            borderRadius: 8,
            backgroundColor: '#F6F8FF',
            paddingHorizontal: 10    
        }}/>
         <View style={{
            width: '100%',
            flexDirection: 'row',
            alignItems: 'center',
            marginTop: 15,
            justifyContent: 'space-between'
        }}/>
         <View style={{
            flexDirection: 'row',
            alignItems: 'center'
        }}/>
        <TouchableOpacity>
        <Image
         source={require('./assets/icon/gmailicon.png')}   
         style={{
         width:20,
         height:20,
         resizeMode:'contain'
         }}
        />    
        </TouchableOpacity>
        <TouchableOpacity>
        <Image
         source={require('./assets/icon/fbiicon.png')}
         style={{
         width:20,
         height:20,
         resizeMode:'contain'
         }}
        />    
        </TouchableOpacity>
        <TouchableOpacity>
        <Image
         source={require('./assets/icon/twittericon.png')}
         style={{
         width:20,
         height:20,
         resizeMode:'contain'
         }}   
        />
        </TouchableOpacity>
        <TouchableOpacity
        style={{
        flexDirection: 'row',
        alignItems: 'center',
        }}>
        <Text style={{
        fontSize: 12, 
        color: '#717171'
        }}>
        Lupa Kata sandi?
        </Text>
        </TouchableOpacity>
        <TouchableOpacity
        style={{
        width: '100%',
        marginTop: 30,
        backgroundColor: '#BB2427',
        borderRadius: 8,
        paddingVertical: 15,
        justifyContent: 'center',
        alignItems: 'center'
        }}
        >
        <Text style={{
        color: '#fff', 
        fontSize: 16, 
        fontWeight: 'bold'
        }}>
        Login
        </Text>
        </TouchableOpacity>
        <View 
        style={{
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        flexDirection: 'row',
        }}>
        <Text style={{
        fontSize: 12, 
        color: '#717171'
        }}>
        Don't Have An Account yet?
        </Text>
        <TouchableOpacity>
        onPress={toregister}
        <Text style={{
        fontSize: 14, 
        color: '#BB2427', 
        marginLeft: 5
        }}>
        Register
        </Text>
        </TouchableOpacity>
        </View>
    </KeyboardAvoidingView>
    </ScrollView>
    </View>
    );
};

export default login;