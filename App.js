import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import authnavigation from './src/authnavigation';
import splashscreen from './screens/splashscreen';
import login from './screens/login';
import register from './screens/register';

const Stack = createNativeStackNavigator();

function routing () {
    return (
    <NavigationContainer>
        <Stack.Navigator>
            screenOptions={{headerShown: false}}
            <Stack.Screen name='splashscreen' component={splashscreen}/>
            <Stack.Screen name='authnavigation' component={authnavigation}/>
            <Stack.Screen name='register' component={register}/>
            <Stack.Screen name='login' component={login}/>
        </Stack.Navigator>
    </NavigationContainer>
    );
};
export default routing;