import React from 'react' ;
import { createStackNavigator } from '@react-navigation/stack';

import login from './screens/login'
import splashscreen from './screens/splashscreen';
import register from './screens/register';

const Stack = createStackNavigator();
function authnavigation(){
    return(
        <Stack.Navigator>
            <Stack.Screen name='login' component={login} />
            <Stack.Screen name='register' component= {register}/>
            <Stack.Screen name='splashscreen' component={splashscreen} />
        </Stack.Navigator>
    );
}
export default authnavigation;