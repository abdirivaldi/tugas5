import React from "react";
import { View, Text, ScrollView, 
         KeyboardAvoidingView, Image, TextInput, 
         TouchableOpacity, StyleSheet,Dimensions } from "react-native";

const register = ({navigation}) => {
    const tologin = () => {
        navigation.navigate('login');
      };
    return (    
    <View>
        <ScrollView>
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}
        <KeyboardAvoidingView>
        behavior="padding"
        keyboardVerticalOffset={-500}
        <Image 
        source={require('./assets/image/homeimage.png')}
        style={{width:Dimensions.get ('window'),
                height:317}}
        />
        <Text>Welcome, please register</Text>
        <View style={{
                width:'100%',
                backgroundColor: '#fff',
                borderTopLeftRadius: 19,
                borderTopRightRadius: 19,
                paddingHorizontal: 20,
                paddingTop: 38,
                marginTop: -20
        }} />
        <Text style={{color:'red', fontWeight:'bold'}}> Nama  </Text>
        <TextInput
        placeholder="Ketik nama Anda"
        style=
        {{
            marginTop: 15,
            width: '100%',
            borderRadius: 8,
            backgroundColor: '#F6F8FF',
            paddingHorizontal: 10
        }}
        />
        <Text style={{color:'red', fontWeight:'bold'}}> Email </Text>
        <TextInput
        placeholder="Ketik alamat email"
        style= {{
            marginTop: 15,
            width: '100%',
            borderRadius: 8,
            backgroundColor: '#F6F8FF',
            paddingHorizontal: 10    
        }}/>
        <Text style={{color:'red', fontWeight:'bold'}}> Ketik kata sandi  </Text>
        <TextInput
        placeholder="kata sandi"
        secureTextEntry={true}
        style=
        {{
            marginTop: 15,
            width: '100%',
            borderRadius: 8,
            backgroundColor: '#F6F8FF',
            paddingHorizontal: 10
        }}
        />
        <Text style={{color:'red', fontWeight:'bold'}}> ketik ulang kata sandi  </Text>
        <TextInput
        placeholder="Konfirmasi sandi"
        secureTextEntry={true}
        style=
        {{
            marginTop: 15,
            width: '100%',
            borderRadius: 8,
            backgroundColor: '#F6F8FF',
            paddingHorizontal: 10
        }}
        />
         <View style={{
            width: '100%',
            flexDirection: 'row',
            alignItems: 'center',
            marginTop: 15,
            justifyContent: 'space-between'
        }}/>
         <View style={{
            flexDirection: 'row',
            alignItems: 'center'
        }}/>
        <TouchableOpacity>
        <Image
         source={require('./assets/icon/gmailicon.png')}   
         style={{
         width:20,
         height:20,
         resizeMode:'contain'
         }}
        />    
        </TouchableOpacity>
        <TouchableOpacity>
        <Image
         source={require('./assets/icon/fbiicon.png')}
         style={{
         width:20,
         height:20,
         resizeMode:'contain'
         }}
        />    
        </TouchableOpacity>
        <TouchableOpacity>
        <Image
         source={require('./assets/icon/twittericon.png')}
         style={{
         width:20,
         height:20,
         resizeMode:'contain'
         }}   
        />
        </TouchableOpacity>
        <TouchableOpacity
        style={{
        flexDirection: 'row',
        alignItems: 'center',
        }}>
        <Text style={{
        fontSize: 12, 
        color: '#717171'
        }}>
        Sudah punya akun? Login di sini
        </Text>
        </TouchableOpacity>
        <TouchableOpacity
        style={{
        width: '100%',
        marginTop: 30,
        backgroundColor: '#BB2427',
        borderRadius: 8,
        paddingVertical: 15,
        justifyContent: 'center',
        alignItems: 'center'
        }}
        >
        <Text style={{
        color: '#fff', 
        fontSize: 16, 
        fontWeight: 'bold'
        }}>
        Login
        </Text>
        </TouchableOpacity>
        <View 
        style={{
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        flexDirection: 'row',
        }}>
        <Text style={{
        fontSize: 12, 
        color: '#717171'
        }}>
        sudah punya akun?
        </Text>
        <TouchableOpacity>
        <Text style={{
        fontSize: 14, 
        color: '#BB2427', 
        marginLeft: 5
        }}
        onPress={tologin}>
        login
        </Text>
        </TouchableOpacity>
        </View>
    </KeyboardAvoidingView>
    </ScrollView>
    </View>
    );
};

export default register;